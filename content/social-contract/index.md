---
title: "Social Contract"
---
We shall adhere to our Social Contract:

1. We will put rights of users above profit.

2. Our products will always be committed to user privacy.

3. Our apps and services will always respect users' freedom (100% Free Software app and service).

4. Our service will be interoperable using a free standard (federated with any [XMPP](https://xmpp.org/about/technology-overview/) service). No one will be forced to use our app or service to talk to users of our service. Our users can switch to any other service provider without losing the ability to talk to their existing contacts (similar to sim cards provided by telecom operators).

5. We will donate, whenever possible, to the projects we rely on.
