---
title: 'About'
date: 2018-02-22T17:01:34+07:00
draft: false
---
Our main goals are:

- The app will respect users' freedom, called [Free/Swatantra Software](https://www.gnu.org/philosophy/free-software-even-more-important.html), and will be controlled by users;

- The app will respect your privacy;

- The chatting service is funded directly by the users based on subscription fee, rather than selling their data;

- Ensuring a democratic decision making structure by registering it as a Multi State Cooperative Society.

- In addition, we will follow the [Social Contract](/social-contract) and [Code of Conduct](/coc).

Currently WhatsApp has a monopoly in instant messaging and many people are forced to use it even if they don't like the privacy policy or terms of use. We aim to provide a messaging app that [respects users' freedom](https://www.gnu.org/philosophy/free-software-even-more-important.html) and is interoperable (users can choose from many service providers) with democratic decision making structure as a [Multi State Cooperative Society](https://mscs.dac.gov.in). 

To register as a cooperative society, we need to find 50 members each from at least two Indian states. We are yet to decide the final cost of a share, but tentatively looking at 1000 rupees (we can fix a smaller amount too if we feel some people can't afford it). Since we do not want to collect any user data, the service will be funded directly by users in the form of subscription,which can be monthly or yearly.

Our initial estimate suggests that if we get 10,000 users, then 200 rupees per user for 3 years is enough to run the service and pay the employees. The more the number of users, the lesser the subscription cost per user, and vice-versa. We would like to invite people who want to support this project to challenge the monopoly of WhatsApp, [to become members](/become-a-member), to buy shares and help find more members. 

This is a project in users interest. Naturally, more involvement from users will be required for this to become successful. We already have the required software (this will be based on the [Quicksy app](https://quicksy.im), which is Free Software), expertise and experience to provide such service (We are running https://poddery.com is running over 8 years, which provides very similar service).

<style>
.button  {background-color: #ff1493;}
</style
<div>
<a href="/become-a-member"><button class="button">Become A Member</button></a>
</div>
