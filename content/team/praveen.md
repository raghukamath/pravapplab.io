---
title: 'Pirate Praveen'
date: 2022-03-03 
draft: false
image: '/images/praveen.jpeg'
jobtitle: Member
weight: 2
---
A skilled labourer in a social emergency campaigning to remake this world as he sees fit (a pirate). For a world where everyone have enough to survive and can do what they really love to do as opposed to what pay them the most.

“The acquisition of wealth is no longer the driving force in our lives. We work to better ourselves and the rest of humanity.” - Jean Luc Picard, Star Trek First Contact.

A [Debian](https://debian.org) Developer, privacy advocate and a politician. Mentors students to contribute to [Free Software](https://www.gnu.org/philosophy/free-software-even-more-important.html).
