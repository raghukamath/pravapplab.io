---
title: "iOS app recommendations for XMPP"
date: 2022-03-11T00:54:07+05:30
draft: false
---
These are the XMPP apps that we recommend for iOS users.

### Snikket

If you already have an XMPP account, for example [disroot.org](https://disroot.org), [poddery.com](https://poddery.com), [diasp.in](https://diasp.in) etc, then Snikket app is the best bet. 

Services like [chinwag.im](https://chinwag.im/), [xmpp.cloud](https://xmpp.cloud/) and [disroot.org](https://disroot.org) allow signing up via browser. [poddery.com](https://poddery.com) and [diasp.in](https://diasp.in) also allow sign up via browser but sign ups are temporarily closed.

### Siskin IM

Siskin IM app allows creating account but a few things need adjustments: 

1. Services suggested by default needs email confirmation but it is not obvious from the app interface.

2. HTTP upload needs to be enabled by default for asynchronous file sharing. Default is jigle file transfer, which is peer to peer but both users need to be online to transfer files.

3. Subscription needs to be enabled manually after adding a contact. This is sometimes required for omemo encrcontact

4. OMEMO needs to be enabled for each chat/contact from settings.

### Monal IM

The drawback is that support for encrypted groups are still missing.
