---
title: 'Homepage'
meta_title: 'Prav App'
description: "Prav- privacy respecting chat app"
intro_image: ""
intro_image_absolute: true
intro_image_hide_on_mobile: true
---

# Prav - Privacy Respecting Chatting App 

Prav is a social project to get a lot of people to invest small amounts to run an interoperable messaging service that will respect users' [freedom](https://www.gnu.org/philosophy/free-software-even-more-important.html) and privacy. 

It will be based on [Quicksy](https://quicksy.im) which gives the same convenience as WhatsApp without locking users to its service by interoperability with any [XMPP](https://xmpp.org) service provider. Prav app will be funded directly by users in form of subscription fee.

We plan to offer this service under a Multi State Cooperative Society in India.

We need 50 members from at least two Indian states to register as a cooperative, which marks our priority before launching the app. Diversity among our members is another top priority to increase representation from every community.

<style>
.button  
{
background-color: #ff1493;
}
</style
<div>
<a href="/become-a-member"><button class="button">Become A Member</button></a>
<br>
<br>
<p><b>Get in touch for any queries or feedback, Contact Email: prav@fsci.in</b></p>
<br>
<p style="font-size:36px"><b>Website Construction In Progress</b></p>

</div>

