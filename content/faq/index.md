---
title: "FAQ"
---

### What is Prav?

Prav is an end-to-end encrypted by default, intuitive & easy-to-use free software instant messenger.

### Why the name 'Prav'?

'Prav' means pigeon in Malayalam, the emblem of universal peace.

### Where can I download the app?

The app is not released yet. Our priority is to find 50 committed people from at least two Indian states each and register as a cooperative society. We are planning to launch the app and the service after we register.

### We already have many messenger apps available, what is the need for another one?

First of all, popular messenger apps like WhatsApp are proprietary software, which is controlled by the developer and that often makes [proprietary software a malware](https://gnu.org/malware), which is then [used for surveillance of users](https://www.gnu.org/proprietary/proprietary-surveillance.html). We want to give users control of the software. Therefore, Prav app will be released as [Free/Swatantra Software](https://www.gnu.org/philosophy/free-software-even-more-important.html), which gives users freedom and control over the software. Here 'free' refers to 'freedom' and not price. Providing source code of the app promotes a culture of transparency, and the code can be audited independently. On the other hand, any trust on proprietary software is a blind trust.

Another major problem is centralization: WhatsApp, Telegram and Signal are all centralized services. They are controlled by one entity. We don't think that the communcations of the whole world relying on a single company is sustainable. Facebook outage [is a reminder of this](https://mashable.com/article/facebook-outage-small-business-impact). We think more and more instant messenger services coming up and interoperable(user of service A can talk to user of service B) is better as users will have more choices of service providers, similar to email services and telecom operators. This will give users real control over the messaging software and services they use. Another way users will have control is the democratic decision making that comes with the structure of a cooperative society.

### I use every app for free? What are you talking about 'Free Software'?

When we use the term 'Free Software', we mean *"Free" as in "Freedom", not "Free of cost"*. Free Software means users have freedom to use, study, modify, share and share the modified versions of the software. Free Software is a matter of liberty and not price. Some examples of Free Software are: Firefox browser, VLC media player, Ubuntu etc. Conversations app is a Free Software, which user needs to pay to install from the Google Play Store.

### Is Prav a paid service, or free of cost?

Prav will be a paid service, but this demands some elaboration. Users can install Prav for no price from app stores but account creation will cost an amount. We don't intend to use your private conversations, nor your metadata to make money. If you want to use free of cost services, you will find many XMPP services that you can use without any fee, and you can still talk to Prav users. One example is Quicksy app for Android.

### How much will the account cost?

Currently, the mark has been set for ₹200 per user for a period of 3 years. That means users will have to renew their subscriptions every 3 years. However, this is a tentative decision and will depend upon how many users subscribe to our service.

### Wait! What will I do with Prav app if I don't want to pay for the account? I see this is a trick of yours to bind us into your service with your app.

Dear user, please note that this is not the case. As per our [**Social Contract**,](https://prav.app/social-contract//) our app is designed to allow you choose the XMPP service of your choice. We, the ones behind Prav are committed to make people know that instant messengers don't have to lock-in their users. Likewise, and conversely, Prav accounts can be used on other apps that support XMPP. 

### What can I do with Prav? What are the features? 

Of course! Prav will have all the features that you expect from an instant messenger. It has file sharing, audio-video calls, contact discovery, and of course, messages will be end-to-end encrypted by default!

### So, Prav runs on XMPP. What is this indeed? 

[XMPP](https://en.wikipedia.org/wiki/XMPP) is an open protocol for exchange of information in real-time, and to be simple, it does so by the exchange of data in XML formats. As such, you can rest assured that all your data, and your metadata, is in such a protocol which is open to verification, modification and extension. Add on top of that another open encryption protocol: [OMEMO](https://conversations.im/omemo/) and you might have understood by now why Prav is a very secure and privacy-respecting messenger!
